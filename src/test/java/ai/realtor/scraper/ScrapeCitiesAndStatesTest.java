package ai.realtor.scraper;

import ai.realtor.utilities.DriverConfig;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeSuite;

public class ScrapeCitiesAndStatesTest extends DriverConfig {
    Gson gson = new GsonBuilder().setPrettyPrinting().create();

    private String BaseURL;

    @BeforeSuite
    public void before() {
        BaseURL = System.getProperty("inputParameters.baseURL", "https://www.realtor.com/realestateagents/");
    }

    @Test
    public void getRealtors() {
        int n = 0;

        while (n < 20) {
            System.out.println("=============");
            System.out.println("[" + BaseURL + "] SHARED_ORDER_PATH: " + System.getProperty("SHARED_ORDER_PATH"));
            System.out.println("[" + BaseURL + "] ARTIFACTS_PATH:    " + System.getProperty("ARTIFACTS_PATH"));
            System.out.println("[" + BaseURL + "] SCREENSHOTS_PATH:  " + System.getProperty("SCREENSHOTS_PATH"));
            System.out.println("=============");

            try {
                Thread.sleep(5000);
            } catch (Exception e) {
                System.out.println("sleeping failed");
            }

            n++;
        }
    }
}
